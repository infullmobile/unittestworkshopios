//
//  ex2UnitTest.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 06.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import XCTest
@testable import UnitTestWorkshop

/* How do I test framework specific things? */
/* If I write anything in my unit tests is it okay? */

class ViewControllerUnitTest: XCTestCase {
    
    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        _ = viewController.view
    }
    
    //FIXME What's wrong with this test?
    func testShouldShowPrice() {
        // given
        let expectedPrice = "10,00\u{00a0}zł" // you have to use non braking space !!!!!
        
        // when
        viewController.show(price: 10.00)
        
        // then
        XCTAssertTrue(viewController.priceView.text == expectedPrice)
    }
}
