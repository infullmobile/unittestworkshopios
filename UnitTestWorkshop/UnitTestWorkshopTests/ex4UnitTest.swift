//
//  ex4UnitTest.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import XCTest
@testable import UnitTestWorkshop

class MagicViewControllerUnitTest: XCTestCase {
    
    
   var magicViewController: MagicViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        magicViewController = storyboard.instantiateViewController(withIdentifier: "MagicViewController") as! MagicViewController
        _ = magicViewController.view
    }
    
    func testShouldShowPrice() {
        // when
        magicViewController.showPrice()
        
        //TODO this is empty because the service method returns 5 seconds later
        // then
        //XCTAssertFalse((magicViewController.priceView.text ?? "").isEmpty)
    }

    
}
