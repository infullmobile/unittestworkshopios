//
//  ex3UnitTest.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import XCTest
@testable import UnitTestWorkshop

/* Can I try TDD please ? */
/* Is it okay if I don't care about corner cases ? */

class ClientRegistryDbEntityUnitTest: XCTestCase {
    
    
    func testShouldBuildClientRegistryFromArrayOfIds() {
        // given
        let ids = [1, 2, 3]
        
        // when
        let clientRegistryDbEntity = ClientRegistryDbEntity.from(ids)
        
        // then
        XCTAssertTrue(clientRegistryDbEntity.idsSeparetedByComma == "1,2,3")
    }
    
//    func testShouldCombineTwoRegistries() {
//        // given
//        let idSetA = [1, 2, 3]
//        let idSetB = [4, 5, 6]
//        let registryA = ClientRegistryDbEntity.from(idSetA)
//        let registryB = ClientRegistryDbEntity.from(idSetB)
//        
//        // when
//        let registryC = registryA.plus(registryB)
//        
//        // then
//        XCTAssertTrue(registryC.idsSeparetedByComma == "1,2,3,4,5,6")
//    }
    
//    func testShouldCombineTwoRegistriesWhenTheFirstOneIsEmpty() {
//        // given
//        let emptySet = [Int]()
//        let idSetB = [4, 5, 6]
//        let registryA = ClientRegistryDbEntity.from(emptySet)
//        let registryB = ClientRegistryDbEntity.from(idSetB)
//        
//        // when
//        let registryC = registryA.plus(registryB)
//        
//        // then
//        XCTAssertTrue(registryC.idsSeparetedByComma == "4,5,6")
//    }
    
//    func testShouldCombineTwoRegistriesWhenTheSecondOneIsEmpty() {
//        // given
//        let idSetA = [1, 2, 3]
//        let emptySet = [Int]()
//        let registryA = ClientRegistryDbEntity.from(idSetA)
//        let registryB = ClientRegistryDbEntity.from(emptySet)
//        
//        // when
//        let registryC = registryA.plus(registryB)
//        
//        // then
//        XCTAssertTrue(registryC.idsSeparetedByComma == "1,2,3")
//    }
    
//    func testShouldCombineTwoRegistriesWhenBothAreEmpty() {
//        // given
//        let emptySetA = [Int]()
//        let emptySetB = [Int]()
//        let registryA = ClientRegistryDbEntity.from(emptySetA)
//        let registryB = ClientRegistryDbEntity.from(emptySetB)
//        
//        // when
//        let registryC = registryA.plus(registryB)
//        
//        // then
//        XCTAssertTrue(registryC.idsSeparetedByComma == "")
//    }

}
