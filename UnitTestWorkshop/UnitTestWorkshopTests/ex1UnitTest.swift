//
//  UnitTestWorkshopTests.swift
//  UnitTestWorkshopTests
//
//  Created by Krzysztof Turek on 06.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import XCTest
@testable import UnitTestWorkshop

/* How do we test? */
/* What do we test? */
/* When is it enough ? */

class PolishCurrencyFormatterUnitTest: XCTestCase {
    
    private let currencyFormatter = PolishCurrencyFormatter()
    
    func testShouldNotAddSpacesWhenPriceLowerThanThousand() {
        // test whether the formatter formats price bigger than 999 and adds spaces
        // example price: "136657"
        
        // given
        
        // when
        
        // then
    }
    
    func testShouldAddSpacesWhenPriceHigherThanThousands() {
        // test whether the formatter formats price lower than 999
        // example price: "136"
        
        // given
        
        // when
        
        // then
    }
    
    func testShouldSetTwoDecimalSpacesWhenPriceHasLessThanTwoDecimalSpaces() {
        // test whether the formatter always adds two decimal spaces
        // example price "1.0"
        
        // given
        
        // when
        
        // then
    }
    
    func testShouldSetTwoDecimalSpacesWhenPriceHasMoreThanTwoDecimalSpaces() {
        // test whether the formatter always adds two decimal spaces
        // have we thought about everything ?
        // example price : "1.99898"
        
        // given
        
        // when
        
        // then
    }
}


//TODO Things to think about:
// How many bugs have we found in such small class?
//Have tested everything?
//FIXME What about values lower than 1?
//FIXME What about negative values?
