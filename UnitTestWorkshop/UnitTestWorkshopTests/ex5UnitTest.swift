//
//  ex5UnitTest.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import XCTest
@testable import UnitTestWorkshop

/* Someone wrote these tests and they break all the time, can I just remove them ? */

class ItemUnitTest: XCTestCase {
    

    func testShouldReturnOriginalMultipliedByQuantityPriceIfDiscountPriceUnavailable() {
        // given
        let product = ProductFactory.productWithoutDiscountPrice()
        let quantity = 2
        let item = getItem(product: product, quantity: quantity)
        let expectedPrice = product.originalTonsPrice * Decimal(quantity)
        
        // when
        let onlinePrice = item.onlinePrice
        
        // then
        XCTAssertTrue(onlinePrice == expectedPrice)
    }
    
    func testShouldReturnDiscountMultipliedByQuantityPriceIfDiscountPriceAvailable() {
        // given
        let product = ProductFactory.productWithDiscountPrice()
        let quantity = 3
        let item = getItem(product: product, quantity: quantity)
        let expectedPrice = product.discountTonsPrice * Decimal(quantity)
        
        // when
        let onlinePrice = item.onlinePrice
        
        // then
        XCTAssertTrue(onlinePrice == expectedPrice)
    }
    
    
    func getItem(product: Product, quantity: Int) -> Item {
        return Item(id: 1, quantity: quantity, product: product)
    }
}

class OrderTest: XCTestCase {
    
    func testShouldCalculateSubTotalOfAllItemsInOrder() {
        // given
        let quantityA = 2
        let productA = ProductFactory.productWithDiscountPrice()
        let itemA = Item(id: 1, quantity: quantityA, product: productA)
        
        let quantityB = 3
        let productB = ProductFactory.productWithoutDiscountPrice()
        let itemB = Item(id: 1, quantity: quantityB, product: productB)
        
        let expectedSubtotal = productA.discountTonsPrice * Decimal(quantityA) + productB.originalTonsPrice * Decimal(quantityB)
        let order = Order(items: [itemA, itemB])
        
        // when
        let subTotal = order.subTotalPrice
        
        // then
        XCTAssertTrue(subTotal == expectedSubtotal)
    }
}


struct ProductFactory {
    
    private init() {}
    
    static func productWithDiscountPrice() -> Product {
        return Product(id: 1,
                       name: "productA",
                       originalTonsPrice: 2,
                       discountTonsPrice: 3,
                       retailerPrice: 4)
    }
    
    static func productWithoutDiscountPrice() -> Product {
        return Product(id: 1,
                       name: "productA",
                       originalTonsPrice: 5,
                       discountTonsPrice: -1,
                       retailerPrice: 6)
    }
}
