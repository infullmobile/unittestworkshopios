//
//  ClientRegistryDbEntity.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

struct ClientRegistryDbEntity {
    
    let idsSeparetedByComma: String
}

extension ClientRegistryDbEntity {
    
    static func from(_ listOfIds: [Int]) -> ClientRegistryDbEntity {
        return ClientRegistryDbEntity(idsSeparetedByComma: listOfIds.map { String($0) }.joined(separator: ","))
    }
    
    // TODO add method plus()
}
