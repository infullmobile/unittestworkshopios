//
//  PolishCurrencyFormatter.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 06.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

struct PolishCurrencyFormatter: CurrencyFormatter {
    
    private let currencyFormatter = NumberFormatter()
    
    init() {
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.groupingSeparator = " "
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "pl_PL")
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
    }
    
    func format(price: Decimal) -> String {
        return currencyFormatter.string(for: price) ?? "?"
    }
}
