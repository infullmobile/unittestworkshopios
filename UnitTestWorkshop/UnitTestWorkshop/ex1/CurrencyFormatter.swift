//
//  CurrencyFormatter.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 06.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

protocol CurrencyFormatter {
    func format(price: Decimal) -> String
}
