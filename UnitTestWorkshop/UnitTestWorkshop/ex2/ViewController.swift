//
//  ViewController.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 06.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var priceView: UILabel!
    
    func show(price: Decimal) {
        priceView.text = PolishCurrencyFormatter().format(price: price)
    }


}

