//
//  Service.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

protocol Service {
    func fetchPrice(completionHandler: @escaping ((String) -> Void))
}
