//
//  MagicViewController.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import UIKit

class MagicViewController: UIViewController {

    @IBOutlet weak var priceView: UILabel!
    
    func showPrice() {
        MagicService().fetchPrice { [weak self] price in
            
            guard let vc = self else { return }
            
            vc.priceView.text = price
        }
    }
}
