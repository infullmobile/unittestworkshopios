//
//  MagicService.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation
import UIKit

struct MagicService: Service {
    
    func fetchPrice(completionHandler: @escaping ((String) -> Void)) {

        DispatchQueue.global(qos: .background).async {
            let price = MyTimeTakingOperations().getPrice()
            
            DispatchQueue.main.async {
                completionHandler(price)
            }
        }
    }
}
