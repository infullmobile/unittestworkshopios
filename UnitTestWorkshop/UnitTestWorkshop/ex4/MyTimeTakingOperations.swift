//
//  MyTimeTakingOperations.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

struct MyTimeTakingOperations {
    
    func getPrice() -> String {
        sleep(5)
        return "5.09"
    }
}
