//
//  Item.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

struct Item {
    
    let id: Int
    let quantity: Int
    let product: Product
}

extension Item {
    
    var onlinePrice: Decimal {
        return product.productOnlinePrice * Decimal(quantity)
    }
    
    var retailerPrice: Decimal {
        return product.retailerPrice
    }
}
