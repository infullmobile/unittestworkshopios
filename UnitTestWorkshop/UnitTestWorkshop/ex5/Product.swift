//
//  Product.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

struct Product {
    
    let id: Int
    let name: String
    let originalTonsPrice: Decimal
    let discountTonsPrice: Decimal
    let retailerPrice: Decimal
}

extension Product {
    
    var productOnlinePrice: Decimal {
        return discountTonsPrice > 0 ? discountTonsPrice : originalTonsPrice
    }
}
