//
//  Order.swift
//  UnitTestWorkshop
//
//  Created by Krzysztof Turek on 07.02.2017.
//  Copyright © 2017 Krzysztof Turek. All rights reserved.
//

import Foundation

struct Order {
    
    let items: [Item]
}

extension Order {
    
    var subTotalPrice: Decimal {
        return items.map { $0.onlinePrice }.reduce(0, +)
    }
}
